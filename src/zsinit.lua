local ZeroScripts = getgenv().ZeroScripts

if ZeroScripts then
    return
end

ZeroScripts = {}
getgenv().ZeroScripts = ZeroScripts

local zsinit = ZeroScripts.zsinit
zsinit = {}
zsinit.__version__ = "0.1.1"
ZeroScripts.zsinit = zsinit
